package trigopaulo;
import robocode.*;
import java.awt.Color;
public class NinjadoArrocha extends AdvancedRobot
{

	public void run() {

		 setBodyColor(new Color(128, 128, 50));
		 setGunColor(new Color(128, 128, 50));
		 setRadarColor(Color.green);
		 setScanColor(new Color(128, 128, 50));
		 
		while(true) {
			setAhead(200);
			setTurnGunRight(90);
			setTurnLeft(30);	
			execute();
		}
	}
	public void onScannedRobot(ScannedRobotEvent e) {
		double dist = e.getDistance();
		double pow = e.getEnergy();
		if (dist <150 && pow > 40){
			fire(3);
		 }	if (dist <150 && pow < 40){
		      fire(2);
			  }if (dist <150 && pow <20){
			      fire(1);
		 } if ( dist >150 && dist <170){
		fire(1);
		}else{
		setBack(100);
		}
		}
	public void onHitByBullet(HitByBulletEvent e) {
		   	double ang = e.getBearing();
			setTurnRight(ang);
			 setTurnRight(20);
			 ahead(100);
             scan();
                 }
	public void onHitWall(HitWallEvent e) {
		double ang = e.getBearing();
		    setTurnRight(ang);
			setTurnRight(10);
			back(100);
            scan();}
			
	public void onHitRobot(HitRobotEvent e){
		double ang = e.getBearing();
		if (ang > -10 && ang < 10) {
           setTurnGunRight(ang);
			fire(2);
			back(30);
			}if (e.isMyFault()){
		     turnRight(10);
		}
	}	
	}
