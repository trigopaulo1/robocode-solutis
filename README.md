# Solutis Robot Arena 2020
Por Paulo Trigo

## Sobre o Robô
 Movimentar-se constantemente,economizar energia,explorar as possibilidades. Esses pensamentos nortearam a criação do meu robô, o NinjadoArrocha, que apesar do nome cômico, baseado no grupo sergipano de arrocha "Ninjas do Arrocha", é fruto de dedicação e muito aprendizado na área da programação e desenvolvimento de softwares. Ao pesquisar sobre o Robocode, assistir aulas e ler comentários na internet, percebi que já existem muitos robôs focados no ataque, seguir os adversários, ou simplesmente, "sair atirando". Sendo assim, resolvi fazer diferente, uma máquina que tenha boa movimentação e procure atirar nos melhores momentos possíveis. É dessa forma que pensei para programar o deslocamento base do NinjadoArrocha, seguindo um padrão de navegação circular, evitando andar em linha reta e sendo menos previsível que outros programas que seguem essa linha de se deslocar pelo mapa. Para a circularidade do movimento e a diversidade (ou loucura) nos movimentos, analisei dois robôs que gostei muito, o "Crazy" e o "SpinBot".

 Desenvolvi também uma forma eficiente de poupar energia em meu programa, atirar na hora certa e somente quando necessário. Fiz isso unindo os dados fornecidos pelos códigos "e.getEnergy()" e "e.getDistance()" (que chamei de pow e dist em meu código), caso NinjadoArrocha detecte outro em uma distância menor que 150 pixels ele atira, a potência do tiro varia com a sua energia, poupando-a de uma forma inteligente. Acima de 40 pontos, detectado o adversário, a potência do disparo será 3, entre 40 e 20 pontos, a potência será igual a 2, abaixo dos 20 pontos, potência de apenas 1, visando não gastar o HP de meu robô. Caso os critérios não sejam atendidos, ele recua, para que não seja alvejado por outros tanques.

 Nesse sentido, resolvi explorar as possibilidades que a aplicação e o campo de batalha oferecem, procurando mudar de local rapidamente caso bata em uma parede, em outro robô, ou seja atingido por uma bala. Mudando a sua localização em campo de batalha, o NinjadoArrocha se torna cada vez menos previsível, além de evitar perder pontos por colidir diversas vezes nas paredes,ser alvo fácil por não conseguir alterar seu posicionamento, ou ainda ser atacado por outras máquinas, especialmente as mais ofensivas e que atacam muito fortemente ao sentir a presença de outro robô nos arredores, que é o caso do "Tracker", que persegue os adversários e alveja-os quando está próximo deles, ou então do "TrackFire".

## Pontos positivos 
- Mobilidade
- Economia de energia
- Versatilidade (principalmente com uso dos comandos "if" e "else")

## Pontos Negativos
### Encurralado
 É uma boa forma de explicar os dois maiores problemas que encontrei no NinjadoArrocha quando estava a programá-lo e nos testes que fiz com ele. 
 #### Encurralado (Canto - Adversário)
 Aconteceu pouquíssimas vezes nas batalhas que simulei contra outros robôs.Porém, ao ficar preso em um canto da parede e ser atingido por disparos inimigos, NinjadoArrocha passa a ser um mero alvo, sendo facilmente acertado e sem perspectiva de fuga.
 #### Encurralado (Múltiplos Adversários)
 Esse ponto foi um complicador bem maior nas primeiras versões do código do NinjadoArrocha, mas ainda é um problema, amenizado, só que ainda um problema. Ao estar entre muitos inimigos, meu robô perde muita energia, uma vez que é acertado pelo "fogo" dos outros robôs. Porém, trabalhei em formas para que ele não fosse passivo e atingisse outros competidores (principalmente na situação "onHitRobot" e "onHitByBullet").

 ## Minha Experiência
 Tenho 18 anos de idade, e até essa etapa do processo, não tinha nenhuma experiência com programação em sí, apenas interesse e fascínio pela área (que se mostrou ainda mais intenso após aprender a usar o robocode, a linguagem Java e pesquisar sobre desenvolvimento de softwares). Tive que aprender Introdução à Lógica de Programação, Introdução à Programação em Java e Robocode em um curto período de tempo. A partir do momento que me senti desafiado para projetar um robô vencedor, busquei verificar o "modus operandi" dos oponentes, afim de criar uma solução nova para tornar NinjadoArrocha um programa eficiente e assertivo na maioria dos confrontos. Dessa forma, me sinto encorajado para participar de novos desafios, não somente no campo da Tecnologia da Informação, mas na vida como um todo.
